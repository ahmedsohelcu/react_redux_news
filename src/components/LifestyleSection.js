import {Fragment} from 'react'
const LefestyleSection = ()=>{
    return(
        <Fragment>
            <div className="title-section">
                <h1><span className="world">Lifestyle</span></h1>
            </div>
            <div className="owl-wrapper">
                <div className="owl-carousel" data-num={1}>
                    <div className="item">
                        <ul className="list-posts">
                            <li>
                                <img src="/assets/upload/news-posts/list7.jpg" alt="" />
                                <div className="post-content">
                                    <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <img src="/assets/upload/news-posts/list8.jpg" alt="" />
                                <div className="post-content">
                                    <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <img src="/assets/upload/news-posts/list9.jpg" alt="" />
                                <div className="post-content">
                                    <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="item">
                        <ul className="list-posts">
                            <li>
                                <img src="/assets/upload/news-posts/list2.jpg" alt="" />
                                <div className="post-content">
                                    <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <img src="/assets/upload/news-posts/list6.jpg" alt="" />
                                <div className="post-content">
                                    <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <img src="/assets/upload/news-posts/list1.jpg" alt="" />
                                <div className="post-content">
                                    <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default LefestyleSection;