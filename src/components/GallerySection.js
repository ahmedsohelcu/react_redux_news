import {Fragment} from 'react'
const GallerySection= ()=>{
    return(
       <Fragment>
           {/* carousel box */}
           <div className="carousel-box owl-wrapper">
               <div className="title-section">
                   <h1><span>Gallery</span></h1>
               </div>
               <div className="owl-carousel" data-num={4}>
                   <div className="item news-post image-post3">
                       <img src="/assets/upload/news-posts/gal1.jpg" alt="" />
                       <div className="hover-box">
                           <h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros.</a></h2>
                           <ul className="post-tags">
                               <li><i className="fa fa-clock-o" />27 may 2013</li>
                           </ul>
                       </div>
                   </div>
                   <div className="item news-post image-post3">
                       <img src="/assets/upload/news-posts/gal2.jpg" alt="" />
                       <div className="hover-box">
                           <h2><a href="single-post.html">Nullam malesuada erat ut turpis. </a></h2>
                           <ul className="post-tags">
                               <li><i className="fa fa-clock-o" />27 may 2013</li>
                           </ul>
                       </div>
                   </div>
                   <div className="item news-post image-post3">
                       <img src="/assets/upload/news-posts/gal3.jpg" alt="" />
                       <div className="hover-box">
                           <h2><a href="single-post.html">Suspendisse urna nibh.</a></h2>
                           <ul className="post-tags">
                               <li><i className="fa fa-clock-o" />27 may 2013</li>
                           </ul>
                       </div>
                   </div>
                   <div className="item news-post image-post3">
                       <img src="/assets/upload/news-posts/gal4.jpg" alt="" />
                       <div className="hover-box">
                           <h2><a href="single-post.html">Donec nec justo eget felis facilisis fermentum. Aliquam </a></h2>
                           <ul className="post-tags">
                               <li><i className="fa fa-clock-o" />27 may 2013</li>
                           </ul>
                       </div>
                   </div>
                   <div className="item news-post image-post3">
                       <img src="/assets/upload/news-posts/gal1.jpg" alt="" />
                       <div className="hover-box">
                           <h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros.</a></h2>
                           <ul className="post-tags">
                               <li><i className="fa fa-clock-o" />27 may 2013</li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
           {/* End carousel box */}
       </Fragment>
    )
}

export default GallerySection