import React,{Fragment,useEffect,useState} from "react";
import axios from 'axios';
import LoaderComponent from './LoaderComponent'
import loaderImg from './../loader.gif'
// import {connect} from "react-redux";
// import {fetchNews} from "../actions/newsActions";

const Footer =(props)=>{
    const [categories, setCategories] = useState([]);
    const [galleries, setGalleries] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    // useEffect(() => {
    //     console.log('data from reduxt start')
    //     props.fetchNews();
    //     console.log('data from reduxt end')
    //
    // },[]);

    useEffect(async () =>  {
        setIsLoading(true);
        await axios.get("http://www.featurebangla.com/api/home_news")
            .then((response) => {
                setCategories(response.data.news_categories);
                setGalleries(response.data.photo_galaries);
                console.log(response.data.photo_galaries);
                setIsLoading(false);
            }).catch((error)=> {
                console.log(error.response)
            })
    },[]);

    const newsCategories = (
        categories?.map((news)=>
            <li key={news.id}>
                <a href="#">{news.name} <span>{news.news.length}</span></a>
            </li>
        )

    );

    const newsGalleryData = (
        galleries.map((gallery)=>
            <li key={gallery.id}>
                <a href="#">
                    <img src={`http://featurebangla.com/${gallery.picture}`} />
                </a>
            </li>
        )
    );


    return(
        <Fragment>
            <footer>
                <div className="container">
                    <div className="footer-widgets-part">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="widget text-widget">
                                    <h1>About</h1>
                                    <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis. </p>
                                    <p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. </p>
                                </div>
                                <div className="widget social-widget">
                                    <h1>Stay Connected</h1>
                                    <ul className="social-icons">
                                        <li><a href="#" className="facebook"><i className="fa fa-facebook" /></a></li>
                                        <li><a href="#" className="google"><i className="fa fa-google-plus" /></a></li>
                                        <li><a href="#" className="twitter"><i className="fa fa-twitter" /></a></li>
                                        <li><a href="#" className="youtube"><i className="fa fa-youtube" /></a></li>
                                        <li><a href="#" className="instagram"><i className="fa fa-instagram" /></a></li>
                                        <li><a href="#" className="linkedin"><i className="fa fa-linkedin" /></a></li>
                                        <li><a href="#" className="vimeo"><i className="fa fa-vimeo-square" /></a></li>
                                        <li><a href="#" className="dribble"><i className="fa fa-dribbble" /></a></li>
                                        <li><a href="#" className="pinterest"><i className="fa fa-pinterest" /></a></li>
                                        <li><a href="#" className="flickr"><i className="fa fa-flickr" /></a></li>
                                        <li><a href="#" className="rss"><i className="fa fa-rss" /></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-md-3">
                                <div className="widget posts-widget">
                                    <h1>Random News</h1>
                                    <ul className="list-posts">
                                        <li>
                                            <img src="/assets/upload/news-posts/listw4.jpg" alt="" />
                                            <div className="post-content">
                                                <a href="travel.html">travel</a>
                                                <h2><a href="single-post.html">Pellentesque odio nisi, euismod in ultricies in, diam. </a></h2>
                                                <ul className="post-tags">
                                                    <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="/assets/upload/news-posts/listw1.jpg" alt="" />
                                            <div className="post-content">
                                                <a href="business.html">business</a>
                                                <h2><a href="single-post.html">Sed arcu. Cras consequat.</a></h2>
                                                <ul className="post-tags">
                                                    <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="/assets/upload/news-posts/listw3.jpg" alt="" />
                                            <div className="post-content">
                                                <a href="tech.html">tech</a>
                                                <h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus.</a></h2>
                                                <ul className="post-tags">
                                                    <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            {/*get new categories....*/}
                            <div className="col-md-3">
                                <div className="widget categories-widget">
                                    <h1>News Categories</h1>
                                    <ul className="category-list">
                                        {newsCategories}
                                    </ul>
                                </div>
                            </div>

                            {/*get new news Gallery....*/}
                            <div className="col-md-3">
                                <div className="widget flickr-widget">
                                    <h1>Photo Galleries</h1>
                                    <ul className="flickr-list">
                                        {newsGalleryData}
                                    </ul>
                                    <a href="#">View more photos...</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="footer-last-line">
                        <div className="row">
                            <div className="col-md-6">
                                <p>©ahmedctg</p>
                            </div>
                            <div className="col-md-6">
                                <nav className="footer-nav">
                                    <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="index.html">Purchase Theme</a></li>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </Fragment>
    )
}

export default Footer;

// export default Footer
// export default connect((state)=>({news: state.news.items}),{
//     fetchNews,
// })(Footer);