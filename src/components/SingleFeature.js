import {Fragment,useEffect,useState} from "react";
import axios from 'axios';

const SingleFeature = ()=>{
    return (
        <div className="item news-post standard-post">
        <div className="post-gallery">
            <img src="/assets/upload/news-posts/st2.jpg" alt="" />
            <a className="category-post sport" href="sport.html">Sport</a>
        </div>
        <div className="post-content">
            <h2><a href="single-post.html">Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</a></h2>
            <ul className="post-tags">
                <li><i className="fa fa-clock-o" />27 may 2013</li>
                <li><i className="fa fa-user" />by <a href="#">John Doe</a></li>
                <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
            </ul>
        </div>
    </div>
    )
}

export default SingleFeature;