import {Fragment} from 'react'
const RightSidebarFeaturePost = ()=>{
    return(
        <Fragment>
            <div className="widget features-slide-widget">
                <div className="title-section">
                    <h1><span>Featured Posts</span></h1>
                </div>
                <div className="image-post-slider">
                    <ul className="bxslider">
                        <li>
                            <div className="news-post image-post2">
                                <div className="post-gallery">
                                    <img src="/assets/upload/news-posts/im3.jpg" alt="" />
                                    <div className="hover-box">
                                        <div className="inner-hover">
                                            <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                <li><i className="fa fa-user" />by <a href="#">John Doe</a></li>
                                                <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                                <li><i className="fa fa-eye" />872</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="news-post image-post2">
                                <div className="post-gallery">
                                    <img src="/assets/upload/news-posts/im1.jpg" alt="" />
                                    <div className="hover-box">
                                        <div className="inner-hover">
                                            <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                <li><i className="fa fa-user" />by <a href="#">John Doe</a></li>
                                                <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                                <li><i className="fa fa-eye" />872</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="news-post image-post2">
                                <div className="post-gallery">
                                    <img src="/assets/upload/news-posts/im2.jpg" alt="" />
                                    <div className="hover-box">
                                        <div className="inner-hover">
                                            <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                <li><i className="fa fa-user" />by <a href="#">John Doe</a></li>
                                                <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                                <li><i className="fa fa-eye" />872</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </Fragment>
    );
};

export default RightSidebarFeaturePost;