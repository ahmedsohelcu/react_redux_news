import React,{Fragment,useState,useEffect} from 'react'
import axios from 'axios';
import Loader from './LoaderComponent'
import loader from './../loader.gif'



const WorldSection = ()=>{
    const [isLoading, setIsLoading] = useState(false);
    const [news, setNewsData] = useState(false);

    useEffect(async () =>  {
        setIsLoading(true);
        await axios.get("http://www.featurebangla.com/api/home_news")
            .then((response) => {
                setNewsData(response.data.marquees);
                console.log( 'from test loader');
                console.log(response.data.marquees);
                setIsLoading(false);
            }).catch((error)=> {
                console.log(error.response)
            })
    },[]);


    return(
        <Fragment>
            {/* carousel box */}
            {
                isLoading ? <Loader/>
                    :
                    <div className="worldSection carousel-box owl-wrapper">
                        <div className="title-section">
                            <h1><span className="world">world section</span></h1>
                        </div>
                        <div className="owl-carousel" data-num={2}>
                            <div className="item">
                                <div className="news-post image-post2">
                                    <div className="post-gallery">
                                        <img src="/assets/upload/news-posts/im1.jpg" alt=""/>
                                        <div className="hover-box">
                                            <div className="inner-hover">
                                                <h2><a href="single-post.html">1 Pellentesque odio nisi, euismod in,
                                                    pharetra a, ultricies in, diam. </a></h2>
                                                <ul className="post-tags">
                                                    <li><i className="fa fa-clock-o"/>27 may 2013</li>
                                                    <li><i className="fa fa-user"/>by <a href="#">John Doe</a></li>
                                                    <li><a href="#"><i className="fa fa-comments-o"/><span>23</span></a>
                                                    </li>
                                                    <li><i className="fa fa-eye"/>872</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul className="list-posts">
                                    <li>
                                        <img src="/assets/upload/news-posts/list1.jpg" alt=""/>
                                        <div className="post-content">
                                            <h2><a href="single-post.html"> fdfa Pellentesque odio nisi, euismod in,
                                                pharetra a, ultricies in, diam. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o"/>27 may 2013</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="/assets/upload/news-posts/list2.jpg" alt=""/>
                                        <div className="post-content">
                                            <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra
                                                a, ultricies in, diam. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o"/>27 may 2013</li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="/assets/upload/news-posts/list3.jpg" alt=""/>
                                        <div className="post-content">
                                            <h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra
                                                a, ultricies in, diam. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o"/>27 may 2013</li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

            }
            {/* End carousel box */}
        </Fragment>
    );
};

export default WorldSection;