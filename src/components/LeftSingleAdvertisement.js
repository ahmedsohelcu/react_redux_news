import {Fragment} from 'react'
const LeftSingleAdvertisement = ()=>{
    return(
        <Fragment>
            {/* google addsense */}
            <div className="advertisement">
                <div className="desktop-advert">
                    <span>Advertisement</span>
                    <img src="/assets/upload/addsense/728x90-white.jpg" alt="" />
                </div>
                <div className="tablet-advert">
                    <span>Advertisement</span>
                    <img src="/assets/upload/addsense/468x60-white.jpg" alt="" />
                </div>
                <div className="mobile-advert">
                    <span>Advertisement</span>
                    <img src="/assets/upload/addsense/300x250.jpg" alt="" />
                </div>
            </div>
            {/* End google addsense */}
        </Fragment>
    );
};

export default LeftSingleAdvertisement;