import React,{Fragment,useEffect,useState} from "react";
import Loader from './LoaderComponent'
import axios from "axios";
import "./../App.css";

const PopularTag = ()=>{
    // const [isLoading, setIsLoading] = useState(false);
    // const [popularNews, setPopularNews] = useState([]);
    // const [latest_news, setLatestNews] = useState([]);


    // const [breakingNews, setBreakingNews] = useState([]);

    //
    // function getPoopularNews(){
    //     setIsLoading(true);
    //     axios.get("http://www.featurebangla.com/api/popular_news")
    //         .then((data)=>{
    //             setPopularNews(data.data.news);
    //             console.log(data.data.news);
    //             setIsLoading(false);
    //         })
    //         .catch((error)=> {
    //             console.log(error.response)
    //         })
    // }

    // const popularData = (
    //     popularNews?.map((news)=>
    //         <li  key={news.id}>
    //             <img src={`http://featurebangla.com/${news.picture}`} />
    //             <div className="post-content">
    //                 <h2>
    //                     <a href="`single-post.html#/${news.id}`">
    //                         {news.title}
    //                     </a>
    //                 </h2>
    //                 <ul className="post-tags">
    //                     <li><i className="fa fa-clock-o" />{news.created_at}</li>
    //                 </ul>
    //             </div>
    //         </li>
    //     )
    // );

    //
    // async function getRecentNews(){
    //     setIsLoading(true);
    //     await axios.get("http://www.featurebangla.com/api/recent_news")
    //         .then((data)=>{
    //             setLatestNews(data.data.news);
    //             console.log(data.data.news);
    //             setIsLoading(false);
    //         })
    //         .catch((error)=> {
    //             console.log(error.response)
    //         })
    // }

    // useEffect(()=>{
    //     getPoopularNews();
    //     getRecentNews();
    // },[]);

    return(
        <Fragment>
            <div className="widget tags-widget">
                <div className="title-section">
                    <h1><span>Popular Tags</span></h1>
                </div>
                <ul className="tag-list">
                    <li><a href="#">News</a></li>
                    <li><a href="#">Fashion</a></li>
                    <li><a href="#">Politics</a></li>
                    <li><a href="#">Sport</a></li>
                    <li><a href="#">Food</a></li>
                    <li><a href="#">Videos</a></li>
                    <li><a href="#">Business</a></li>
                    <li><a href="#">Travel</a></li>
                    <li><a href="#">World</a></li>
                    <li><a href="#">Music</a></li>
                </ul>
            </div>
        </Fragment>
    )
}

export default PopularTag;