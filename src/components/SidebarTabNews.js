import React,{Fragment,useEffect,useState} from "react";
import Loader from './LoaderComponent'
import axios from "axios";
import "./../App.css";

// breaking_news
// popular_news
// recent_news
// latest_news

const SidebarTabNews = ()=>{
    const [isLoading, setIsLoading] = useState(false);
    const [popularNews, setPopularNews] = useState([]);
    const [latest_news, setLatestNews] = useState([]);


    const [breakingNews, setBreakingNews] = useState([]);


    function getPoopularNews(){
        setIsLoading(true);
        axios.get("http://www.featurebangla.com/api/popular_news")
            .then((data)=>{
                setPopularNews(data.data.news);
                console.log(data.data.news);
                setIsLoading(false);
            })
            .catch((error)=> {
                console.log(error.response)
            })
    }

    const popularData = (
        popularNews?.map((news)=>
            <li  key={news.id}>
                <img src={`http://featurebangla.com/${news.picture}`} />
                <div className="post-content">
                    <h2>
                        <a href="`single-post.html#/${news.id}`">
                            {news.title}
                        </a>
                    </h2>
                    <ul className="post-tags">
                        <li><i className="fa fa-clock-o" />{news.created_at}</li>
                    </ul>
                </div>
            </li>
        )
    );


    async function getRecentNews(){
        setIsLoading(true);
        await axios.get("http://www.featurebangla.com/api/recent_news")
            .then((data)=>{
                setLatestNews(data.data.news);
                console.log(data.data.news);
                setIsLoading(false);
            })
            .catch((error)=> {
                console.log(error.response)
            })
    }

    const recentNewsData = (
        latest_news?.map((news)=>

            <li key={news.id}>
                <img src={`http://featurebangla.com/${news.picture}`} />
                <div className="post-content">
                    <h2>
                        <a href="`single-post.html#/${news.id}`">
                            {news.title}
                        </a>
                    </h2>
                    <ul className="post-tags">
                        <li><i className="fa fa-clock-o" />{news.created_at}</li>
                    </ul>
                </div>
            </li>
        )
    );



    useEffect(()=>{
        getPoopularNews();
        getRecentNews();
    },[]);

    return(
        <Fragment>
            <div className="widget tab-posts-widget">
                <ul className="nav nav-tabs" id="myTab">
                    <li className="active">
                        <a href="#option1" data-toggle="tab">Popular</a>
                    </li>
                    <li>
                        <a href="#option2" data-toggle="tab">Recent</a>
                    </li>
                    <li>
                        <a href="#option3" data-toggle="tab">Top Reviews</a>
                    </li>
                </ul>
                <div className="tab-content">
                        <div className="tab-pane active" id="option1">
                            {
                                isLoading ? <div className='loaderPosition'><Loader></Loader></div> :
                                <ul className="list-posts">
                                    {popularData}
                                </ul>
                            }
                        </div>

                    <div className="tab-pane" id="option2">
                        {
                            isLoading ? <div className='loaderPosition'><Loader></Loader></div> :
                            <ul className="list-posts">
                                {recentNewsData}
                            </ul>
                        }

                    </div>

                    <div className="tab-pane" id="option3">
                        {
                            isLoading ? <div className='loaderPosition'><Loader></Loader></div> :
                                <ul className="list-posts">
                                    {popularData}
                                </ul>
                        }
                    </div>

                </div>
            </div>
        </Fragment>
    )
}

export default SidebarTabNews;