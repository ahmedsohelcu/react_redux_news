import {Fragment,useEffect,useState} from "react";
import axios from 'axios';
import Loader from './LoaderComponent'

const TickerNewsSection = ()=>{
    const [tickers, setTicker] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const newsCollections = [
        {id:1,category: 'Bangladesh',title: '1. This is news title',created_at:'24-02-2021',author: 'Ahmed Sohel'},
        {id:1,category: 'Sports',title: '2. This is news title 2',created_at:'24-02-2021',author: 'Ahmed Sohel'},
        {id:1,category: 'Politics',title: '3. This is news title 3',created_at:'24-02-2021',author: 'Ahmed Sohel'},
        {id:1,category: 'Environment',title: '4. This is news title 4',created_at:'24-02-2021',author: 'Ahmed Sohel'},
        {id:1,category: 'Economics',title: '5. This is news title 5',created_at:'24-02-2021',author: 'Ahmed Sohel'},
        {id:1,category: 'Education',title: '6. This is news title 6',created_at:'24-02-2021',author: 'Ahmed Sohel'},
        {id:1,category: 'Health',title: '7. This is news title 7',created_at:'24-02-2021',author: 'Ahmed Sohel'},
    ];
    const fetchData = async () => {
        setIsLoading(true);
        const result = await axios("http://www.featurebangla.com/api/get_breaking_news");
        setTicker(result.data);
        setIsLoading(false);
        console.log('found ticker news')
    };
    useEffect(()=>{
        //===========================
        //fetchData();
    //===========================

        // const url = "http://www.featurebangla.com/api/get_breaking_news";
        // const response = await fetch(url);
        // const data = await response.json();
        // setTicker(data)
        // console.log('ticker');
        // console.log(data[0].title);


        // axios.get("http://www.featurebangla.com/api/get_breaking_news")
        //     .then((response) => {
        //         console.log(response.data)
        //         setTicker(response.data);
        //     }).catch((error)=> {
        //         console.log(error.response)
        //     })
    },[]);


    return(
        <Fragment>
            <section className="ticker-news">
                <div className="container">
                    <div className="ticker-news-box">
                        <span className="breaking-news">breaking news</span>
                        <span className="new-news">News</span>
                        <ul id="js-news">
                            {
                                isLoading ? <Loader></Loader>
                                    :
                                    newsCollections.map((ticker)=>
                                        <li className="news-item"><span className="time-news">{ticker.created_at}</span> <a href="#">
                                            {ticker.title}</a> Donec odio. Quisque volutpat
                                            mattis eros...
                                        </li>
                                    )
                            }
                            {/*<li className="news-item"><span className="time-news">11:36 pm</span> <a href="#">Lorem*/}
                            {/*    ipsum dolor sit amet, consectetuer adipiscing elit.</a> Donec odio. Quisque volutpat*/}
                            {/*    mattis eros...*/}
                            {/*</li>*/}
                            {/*<li className="news-item"><span className="time-news">12:40 pm</span> <a href="#">Dëshmitarja*/}
                            {/*    Abrashi: E kam parë Oliverin në turmë,</a> ndërsa neve na shpëtoi “çika Mille”*/}
                            {/*</li>*/}
                            {/*<li className="news-item"><span className="time-news">11:36 pm</span> <a href="#">Franca do*/}
                            {/*    të bashkëpunojë me Kosovën në fushën e shëndetësisë. </a></li>*/}
                            {/*<li className="news-item"><span className="time-news">01:00 am</span> <a href="#">DioGuardi,*/}
                            {/*    kështu e mbrojti Kosovën në Washington, </a> para serbit Vejvoda*/}
                            {/*</li>*/}
                        </ul>
                    </div>
                </div>

            </section>
        </Fragment>
    );
}

export default TickerNewsSection;
