import React,{Fragment,useEffect,useState} from "react";
import ItemsCarousel from 'react-items-carousel';
import axios from 'axios';
import './../custom.css';
import Loader from './LoaderComponent'

import SidebarTabNews from './SidebarTabNews'
import RightAdvertisement from "./RightAdvertisement";

const LatestArticlesSection = ()=>{
    const [articles, setFeatureArtice] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(async ()=>{
        setIsLoading(true);
        await axios.get("http://www.featurebangla.com/api/random/featureBangla6News")
            .then((response) => {
                console.log(response.data.randomNews)
                setFeatureArtice(response.data.randomNews);
                setIsLoading(false);
            }).catch((error)=> {
                console.log(error.response)
            })
    },[]);

    const  articleData = (
        articles?.map((news)=>
            <div key={news.id} className="news-post standard-post2 default-size">
                <div className="post-gallery">
                    <img src={`http://featurebangla.com/${news.picture}`} />
                </div>
                <div className="post-title">
                    <h2><a href="single-post.html">
                        {news.title}
                    </a></h2>
                    <ul className="post-tags">
                        <li><i className="fa fa-clock-o" /> {news.created_at}</li>
                        <li><i className="fa fa-user" />by <a href="#">{news.user_id}</a></li>
                        <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                    </ul>
                </div>
            </div>
        )
    );

    return(
        <Fragment>
            <section className="block-wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-8">
                            {
                                isLoading ? <Loader></Loader>
                           :
                            <div className="block-content">
                                {/* masonry box */}
                                <div className="masonry-box">
                                    <div className="title-section">
                                        <h1><span>Latest Articles</span></h1>
                                    </div>
                                    <div className="latest-articles iso-call">
                                        {articleData}
                                    </div>
                                </div>
                                {/* End masonry box */}



                                {/* pagination box */}
                                <div className="pagination-box">
                                    <ul className="pagination-list">
                                        <li><a className="active" href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><span>...</span></li>
                                        <li><a href="#">9</a></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                    <p>Page 1 of 9</p>
                                </div>
                                {/* End pagination box */}
                            </div>
                            }
                            {/* End block content */}
                        </div>
                        <div className="col-sm-4">
                            {/*<RightAdvertisement/>*/}
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    );
};

export default LatestArticlesSection;
