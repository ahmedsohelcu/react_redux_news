import {Fragment} from "react";

const HeadingNewsSection = ()=>{
    return(
        <Fragment>
            {/* heading-news-section
			================================================== */}
            <section className="heading-news">
                <div className="iso-call heading-news-box">
                    <div className="news-post image-post default-size">
                        <img src="/assets/upload/news-posts/h1.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post travel" href="travel.html">Travel</a>
                                <h2><a href="single-post.html">Lorem ipsum dolor sit amet, consectetuer</a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                    <div className="image-slider snd-size">
                        <span className="top-stories">TOP STORIES</span>
                        <ul className="bxslider">
                            <li>
                                <div className="news-post image-post">
                                    <img src="/assets/upload/news-posts/h2.jpg" alt="" />
                                    <div className="hover-box">
                                        <div className="inner-hover">
                                            <a className="category-post world" href="world.html">Business</a>
                                            <h2><a href="single-post.html">Franca do të bashkëpunojë me Kosovën në ekonomi. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                <li><i className="fa fa-user" />by <a href="#">John Doe</a></li>
                                                <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                                <li><i className="fa fa-eye" />872</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="news-post image-post">
                                    <img src="/assets/upload/news-posts/h7.jpg" alt="" />
                                    <div className="hover-box">
                                        <div className="inner-hover">
                                            <a className="category-post sport" href="sport.html">Sport</a>
                                            <h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                <li><i className="fa fa-user" />by <a href="#">John Doe</a></li>
                                                <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                                <li><i className="fa fa-eye" />872</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="news-post image-post">
                                    <img src="/assets/upload/news-posts/h4.jpg" alt="" />
                                    <div className="hover-box">
                                        <div className="inner-hover">
                                            <a className="category-post sport" href="sport.html">sport</a>
                                            <h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros. </a></h2>
                                            <ul className="post-tags">
                                                <li><i className="fa fa-clock-o" />27 may 2013</li>
                                                <li><i className="fa fa-user" />by <a href="#">John Doe</a></li>
                                                <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                                <li><i className="fa fa-eye" />872</li>
                                            </ul>
                                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="news-post image-post">
                        <img src="/assets/upload/news-posts/h3.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post food" href="food.html">food &amp; Health</a>
                                <h2><a href="single-post.html">Nullam malesuada erat ut turpis.</a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>20</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                    <div className="news-post image-post">
                        <img src="/assets/upload/news-posts/h1.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post travel" href="travel.html">Travel</a>
                                <h2><a href="single-post.html">Lorem ipsum dolor sit amet, consectetuer</a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                    <div className="news-post image-post">
                        <img src="/assets/upload/news-posts/h5.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post sport" href="sport.html">sport</a>
                                <h2><a href="single-post.html">Donec odio. </a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                    <div className="news-post image-post">
                        <img src="/assets/upload/news-posts/h6.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post fashion" href="fashion.html">fashion</a>
                                <h2><a href="single-post.html">Quisque volutpat mattis </a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                    <div className="news-post image-post">
                        <img src="/assets/upload/news-posts/h5.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post sport" href="sport.html">sport</a>
                                <h2><a href="single-post.html">Donec odio. </a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                    <div className="news-post image-post">
                        <img src="/assets/upload/news-posts/h6.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post fashion" href="fashion.html">fashion</a>
                                <h2><a href="single-post.html">Quisque volutpat mattis </a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                    <div className="news-post image-post">
                        <img src="/assets/upload/news-posts/h3.jpg" alt="" />
                        <div className="hover-box">
                            <div className="inner-hover">
                                <a className="category-post food" href="food.html">food &amp; Health</a>
                                <h2><a href="single-post.html">Nullam malesuada erat ut turpis.</a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" /><span>27 may 2013</span></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>20</span></a></li>
                                </ul>
                                <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/* End heading-news-section */}
        </Fragment>
    );
}

export default HeadingNewsSection;