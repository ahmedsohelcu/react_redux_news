import React,{Fragment,useEffect,useState} from "react";
import ItemsCarousel from 'react-items-carousel';
import axios from 'axios';
import Loader from './LoaderComponent'
import loader from './../loader.gif'

const TodaysFeatureSection = ()=>{
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    const chevronWidth = 40;
    const [todaysFeatures, setTodaysFeatures] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(async () =>  {
        setIsLoading(true);
        await axios.get("http://www.featurebangla.com/api/random/featureBangla9News")
            .then((response) => {
                setTodaysFeatures(response.data.randomNews);
                console.log(response.data.randomNews);
                setIsLoading(false);
            }).catch((error)=> {
            console.log(error.response)
        })
    },[]);

    const featureData = (
        todaysFeatures?.map((news)=>
            <div key={news.id} style={{  height: 290, background: '',padding:10  }}>
                <div className="features-today-box owl-wrapfper">
                    <div className="owl-carousdel" dasta-nsum={4}>
                        <div className="item news-post standard-post">
                            <div className="post-gallery">
                                <img src={`http://featurebangla.com/${news.picture}`} />
                                <a className="category-post sport" href="sport.html">
                                    category..
                                </a>
                            </div>
                            <div className="post-content">
                                <h2><a href="single-post.html">{news.title}</a></h2>
                                <ul className="post-tags">
                                    <li><i className="fa fa-clock-o" />{news.created_at}</li>
                                    <li><i className="fa fa-user" />by <a href="#">{news.user_id}</a></li>
                                    <li><a href="#"><i className="fa fa-comments-o" /><span>23</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    );

    return (
        <section className="features-today">
            {
                isLoading ? <Loader></Loader>
                    :
                    <div className="container">
                        <div className="title-section">
                            <h1><span>Today's Featured</span></h1>
                        </div>

                        <div className='container' style={{ padding: `0 ${chevronWidth}px` }}>
                            <ItemsCarousel
                                requestToChangeActive={setActiveItemIndex}
                                activeItemIndex={activeItemIndex}
                                numberOfCards={4}
                                activePosition={'right'}
                                infiniteLoop={true}
                                alwaysShowChevrons={true}
                                gutter={20}
                                leftChevron={<button>{'<'}</button>}
                                rightChevron={<button>{'>'}</button>}
                                outsideChevron
                                chevronWidth={chevronWidth}>

                                {featureData}

                                {/*<div style={{ height: 200, background: '#EEE' }}>Second card</div>*/}
                                {/*<div style={{ height: 200, background: '#EEE' }}>Second card</div>*/}
                            </ItemsCarousel>
                        </div>
                    </div>
            }
        </section>

    )
}

export default TodaysFeatureSection;