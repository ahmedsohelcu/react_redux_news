import {Fragment,useEffect,useState} from "react";
import axios from 'axios';

const FeatureVideoSection = ()=>{
        // const [featureN, setTicker] = useState([]);
        //
        // useEffect( async ()=>  {
        //
        //     await axios.get("http://www.featurebangla.com/api/get_breaking_news")
        //     .then((response) => {
        //         console.log(response.data)
        //         setTicker(response.data);
        //         loader = false
        //
        //     }).catch((error)=> {
        //         console.log(error.response)
        //     })
        //
        // },[]);

    return(
        <Fragment>
            <section className="feature-video">
                <div className="container">
                    <div className="title-section white">
                        <h1><span>Featured Video</span></h1>
                    </div>
                    <div className="features-video-box owl-wrapper">
                        <div className="owl-carousel" data-num={4}>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video1.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Lorem ipsum dolor sit consectetuer adipiscing elit. Donec odio. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video2.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Quisque volutpat mattis eros. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video3.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Nullam malesuada erat ut turpis. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video4.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video1.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Lorem ipsum dolor sit consectetuer adipiscing elit. Donec odio. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video2.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Quisque volutpat mattis eros. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video3.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Nullam malesuada erat ut turpis. </a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="item news-post video-post">
                                <img alt="" src="/assets/upload/news-posts/video4.jpg" />
                                <a href="https://www.youtube.com/watch?v=LL59es7iy8Q" className="video-link"><i className="fa fa-play-circle-o" /></a>
                                <div className="hover-box">
                                    <h2><a href="single-post.html">Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</a></h2>
                                    <ul className="post-tags">
                                        <li><i className="fa fa-clock-o" />27 may 2013</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}

export default FeatureVideoSection;