import React from "react";
import {buyCake} from "../redux";
import {connect} from "react-redux";

function CakeContainer(props) {
    return (
      <div>
          <h2>Redux Practice</h2>
          <h2>Number of cakes {props.numOfCakes}</h2>
          <h2>name {props.name}</h2>
          <button onClick={props.buyCake}>Buy Cake</button>
      </div>
    );
}

const mapStateToprops = state=>{
    return {
        numOfCakes : state.numOfCakes,
        name : state.name
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        buyCake:()=>dispatch(buyCake())
    }
}

export default connect(
    mapStateToprops,
    mapDispatchToProps
)(CakeContainer);
