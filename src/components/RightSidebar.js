import React,{Fragment,useState,useEffect} from 'react'
import StayConnected from "./StayConnected";
import SidebarTabNews from "./SidebarTabNews";
import PopularTag from "./PopularTag";
import RightSidebarFeaturePost from "./RightSidebarFeaturedPosts";
import RightAdvertisement from "./RightAdvertisement";



export const RightSidebar = ()=>{
    return(
        <Fragment>
            { /*start StayConnected */ }
            <StayConnected/>

            <SidebarTabNews/>

            {/*<PopularTag/>*/}
            <PopularTag/>

            <RightSidebarFeaturePost/>

            <RightAdvertisement/>

        </Fragment>
    )
};





