const LoaderComponent = ()=>{
    return(
        <div style={{textAlign:'center'}}>
            <img src="../loader.gif" alt="" />
        </div>
    )
}

export default LoaderComponent;