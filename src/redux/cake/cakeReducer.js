import {BUY_CAKE} from "./cakeTypes";

const initialState = {
    numOfCakes:10,
    name:'Ahmed Sohel',
    email: 'ahmed@gmail.com',
    phone: '01840149651'
}

const cakeReducer = (state=initialState,action)=>{
    switch(action.type){
        case BUY_CAKE:
            return{
            ...state,
                numOfCakes: state.numOfCakes-1
            }
        default: return state
    }
}

export default cakeReducer;