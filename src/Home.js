import { Fragment } from 'react'

import HeadingNewsSection from './components/HeadingNewsSection'
import TickerNewsSection from './components/TickerNewsSection'
import TodaysFeatureSection from './components/TodaysFeatureSection'
import GallerySection from "./components/GallerySection";
import FeatureVideoSection from "./components/FeatureVideoSection";
import LatestArticlesSection from "./components/LatestArticlesSection";
import WorldSection from "./components/worldSection";
import FashionSection from "./components/FashionSection";
import LifestyleSection from "./components/LifestyleSection";
import LeftSingleAdvertisement from "./components/LeftSingleAdvertisement";
import RightAdvertisement from "./components/RightAdvertisement";
import Footer from "./components/Footer";
import Header from "./components/Header";
import RightSidebarFeaturePost from "./components/RightSidebarFeaturedPosts";
import StayConnected from "./components/StayConnected";
import SidebarTabNews from './components/SidebarTabNews'
import ReactCarousel from './components/ReactCarousel'

import CakeContainer from "./components/CakeContainer";
import {Provider} from "react-redux";
import store from './redux/store'

import PopularTag from "./components/PopularTag";
import {RightSidebar} from "./components/RightSidebar";
import TestDesign from "./components/TestDesign";

// import 'bootstrap/dist/css/bootstrap.min.css';
const Home = () => {
    return (
        <Provider store={store}>
            <Fragment >
                { /* Container */ }
                <div id = "container" >
                {/* Header================================================== */}
                    <Header/>
                { /* End Header */ }
                { /*=======================================================*/ }

                <CakeContainer/>

                { /*=======================================================*/ }
                { /*start headingNews section crousel */ }
                    { /*<HeadingNewsSection/>*/ }
                { /*end headingNews section*/ }
                { /*===============================================*/ }

                { /*===============================================*/ }
                <TickerNewsSection/>
                { /*===============================================*/ }
                    {/*<ReactCarousel></ReactCarousel>*/}

                { /*===============================================*/ }
                { /* features-today-section */}
                <TodaysFeatureSection/> { /* End features-today-section */ }
                { /*=======================================================*/ }

                {/* block-wrapper-section
                 ================================================== */}
                <section className = "block-wrapper">
                    <div className = "container">
                    <div className = "row">
                    <div className = "col-sm-8"> { /* block content */ }
                    <div className = "block-content">
                        { /*===============================================*/ }
                    { /* start World News sectin */ }

                    <WorldSection/>

                    <TestDesign/>

                        { /*end World News sectin */ }
                    { /*===============================================*/ }

                    { /*=======================================================*/ }
                    { /* carousel box */ }
                    <GallerySection/>
                    { /* End carousel box */ }
                    { /*=======================================================*/ }

                    { /* grid box */ }
                    <div className="grid-box">
                        <div className="row" >
                                <div className="col-md-6">
                                    { /*===============================================*/ }
                                { /*fashion section start */ }
                                <FashionSection/> { /*end fashion section */ }
                            { /*===============================================*/ }
                            </div>


                            <div className = "col-md-6">
                                { /*===============================================*/ }
                                { /*lifestyle section start */}
                                <LifestyleSection > </LifestyleSection>
                                { /*end lifestyle section */ }
                                { /*===============================================*/ }
                            </div>
                        </div>
                    </div>
                            { /* End grid box */ }


                            { /*===============================================*/ }
                            { /* google addsense */ }
                                <LeftSingleAdvertisement/>
                            { /* End google addsense */ }
                            { /*===============================================*/}
                                </div>
                            { /* End block content */}
                            </div>

                            <div className = "col-sm-4">
                                { /* sidebar start */ }
                                <div className = "sidebar">
                                    { /*===============================================*/ }
                                        <RightSidebar/>
                                </div>
                            { /* End sidebar */ }
                            </div>
                        </div>
                    </div>
                </section>
                { /*===============================================*/ }
                    { /* End block-wrapper-section */ }


                { /*===============================================*/ }
                <FeatureVideoSection/>
                { /* End feature-video-section */ }
                { /*===============================================*/ }


                { /*===============================================*/ }
                { /*start latest Article sectin */ }
                <LatestArticlesSection/> { /*end latest Article sectin */}
                {/*===============================================*/}


                {/*===============================================*/}
                <Footer/>
                { /* End footer */ }
                {/*===============================================*/}
                </div>

                {/* End Container */ }
            </Fragment>

        </Provider>
    );
}

export default Home;