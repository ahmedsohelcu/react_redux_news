import {FETCH_NEWS} from "../types";

export const newsReducer = ((state={},action)=>{
    switch (action.type) {
        case FETCH_NEWS:
            return {items:action.payload};
        default:
            return state;
    }
});